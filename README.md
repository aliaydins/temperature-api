# temperature-api


# Running 

To run temperature-api with Docker, firstly build an image:
```
docker build -t temperature-api .
```
and run 
```
docker run -p 8080:8080 temperature-api
```
test it, with 

```
localhost:8080/
localhost:8080/temperature?city=istanbul
```

# Running with locally

To run with nodemon 
```
npm install 
```
and run 

```
nodemon dev
```

if nodemon not found, install nodemon this command line and try again 

```
npm install nodemon -g --save
```
