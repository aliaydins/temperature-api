import { ResponseCustom } from '../types/request-types';

import axios from 'axios';
let apiKey = 'e69b34d0a4087a6d5903757a8fe9f1db';
let baseUrl = 'https://api.openweathermap.org/data/2.5/weather';

export const getTemperature = async (city?: string): Promise<ResponseCustom> => {
  let r: any;

  try {
    r = await axios.get(`${baseUrl}?q=${city}&appid=${apiKey}&units=metric`);
  } catch (err) {
    return Promise.resolve({ message: 'city not found', status_code: 404 });
  }
  return Promise.resolve({ temperature: r.data.main.temp, status_code: r.status });
};
