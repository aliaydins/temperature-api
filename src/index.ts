import app from './app';
let port = 8080;

app
  .listen(port, () => {
    console.log(`server running on port : ${port}`);
  })
  .on('error', (e: any) => console.log(e));
