FROM node:14 as base

USER root
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
WORKDIR /opt/

RUN mkdir ./app
COPY . ./app

WORKDIR /opt/app
RUN rm ./docker-entrypoint-dev.sh \
  && chgrp -R 0 ./ \
  && chmod -R g=u ./ \
  && chmod +x ./docker-entrypoint.sh \
  && npm install

USER 1001
EXPOSE 8080

CMD ["./docker-entrypoint.sh"]
